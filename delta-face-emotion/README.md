## 0. Môi trường
- NodeJS v16.14.2
- ReactJS

## 1. Install thư viện
```bash
npm install
```

## 2. Chạy
```bash
npm start
```

## 3. Truy cập trang web http://localhost:5002