import React, { useEffect, useState, useRef } from "react";
import * as faceapi from 'face-api.js';
import { Chart as ChartJS, RadialLinearScale, PointElement, LineElement, Filler, Tooltip, Legend } from 'chart.js';
import { Radar } from 'react-chartjs-2';


ChartJS.register(
    RadialLinearScale,
    PointElement,
    LineElement,
    Filler,
    Tooltip,
    Legend
);

const Participant = ({ stream }) => {
    const videoRef = useRef();
    const canvasRef = useRef();

    const TINY_FACE_DETECTOR = 'tiny_face_detector';
    const SSD_MOBILENETV1 = 'ssd_mobilenetv1'
    const selectedFaceDetector = SSD_MOBILENETV1;

    // ssd_mobilenetv1 options
    const minConfidence = 0.5;

    // tiny_face_detector options
    const inputSize = 512;
    const scoreThreshold = 0.5;

    const [modelsLoaded, setModelsLoaded] = useState(false);
    const [captureVideo, setCaptureVideo] = useState(false);
    const [radarData, setRadarData] = useState({
        labels: ['Surprised', 'Disgusted', 'Fearful', 'Sad', 'Angry', 'Happy', 'Neutral'],
        borderColor: 'rgba(255, 255, 255, 1)',
        redraw: true,
        datasets: [
            {
                label: '',
                data: [0, 0, 0, 0, 0, 0, 0],
                backgroundColor: 'rgba(255, 99, 132, 0.5)',
                borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1
            },
        ],
    });

    //== useEffect =============================================================
    useEffect(() => {
        loadModels();
    }, [])

    useEffect(() => {
        if (stream) {
            // video
            const videoTracks = stream.getVideoTracks();
            if (videoTracks.length > 0) {
                videoRef.current.srcObject = new MediaStream([videoTracks[0]]);
                videoRef.current.onloadedmetadata = () => { videoRef.current.play(); };
                setCaptureVideo(true);
            } else {
                if (videoRef?.current) videoRef.current.srcObject = null;
                setCaptureVideo(false);
            }
        } else {
            setCaptureVideo(false);
        }

        return () => {
            // eslint-disable-next-line
            if (videoRef?.current) videoRef.current.srcObject = null;
        }
        // eslint-disable-next-line
    }, [stream])

    useEffect(() => {
        if (modelsLoaded && captureVideo) {
            setTimeout(runFaceExpression);
        }
    }, [modelsLoaded, captureVideo])

    //== FACE API =============================================================
    const getFaceDetectorOptions = () => {
        return selectedFaceDetector === SSD_MOBILENETV1
            ? new faceapi.SsdMobilenetv1Options({ minConfidence })
            : new faceapi.TinyFaceDetectorOptions({ inputSize, scoreThreshold })
    }

    const runFaceExpression = async () => {
        if (canvasRef && canvasRef.current) {
            const options = getFaceDetectorOptions();
            const result = await faceapi.detectSingleFace(videoRef.current, options).withFaceExpressions();

            if (result) {
                console.log(result)
                const dims = faceapi.matchDimensions(canvasRef.current, videoRef.current, true);
                const resizedResult = faceapi.resizeResults(result, dims);
                console.log(resizedResult);
                faceapi.draw.drawDetections(canvasRef.current, resizedResult);
                faceapi.draw.drawFaceExpressions(canvasRef.current, resizedResult, minConfidence);
            } else {

            }

            setTimeout(runFaceExpression);
        }
    }

    const getCurrentFaceDetectionNet = () => {
        if (selectedFaceDetector === SSD_MOBILENETV1) {
            return faceapi.nets.ssdMobilenetv1;
        }
        if (selectedFaceDetector === TINY_FACE_DETECTOR) {
            return faceapi.nets.tinyFaceDetector;
        }
    }

    const isFaceDetectionModelLoaded = () => {
        return !!getCurrentFaceDetectionNet().params;
    }

    const loadModels = async () => {
        if (!isFaceDetectionModelLoaded()) {
            await getCurrentFaceDetectionNet().load('/models');
        }

        const MODEL_URL = '/models';
        await faceapi.nets.faceExpressionNet.loadFromUri(MODEL_URL);
        setModelsLoaded(true);
    }

    //==================== RETURN ===========================================
    return (
        <div className="ani" style={{ display: "", background: "#000", height: "auto", width: "100%", position: "relative", textAlign: "center" }}>

            {/* Participant video */}
            <video
                ref={videoRef}
                autoPlay={true}
                muted={true}
                className="remote-video-par"
                style={{
                    width: "100%",
                    height: "auto",
                    maxHeight: "100vh"
                }}
            />

            <canvas ref={canvasRef}
                className="remote-video-par"
                style={{
                    width: "100%",
                    height: "auto",
                    maxHeight: "100vh"
                }}
            />

            <div style={{
                position: "fixed",
                bottom: "0px",
                right: "0px",
                width: "256px",
                height: "256px",
                background: "rgba(255,255,255, 1)"
            }}>
                <Radar data={radarData} />
            </div>
        </div>
    );
};

export default Participant;
