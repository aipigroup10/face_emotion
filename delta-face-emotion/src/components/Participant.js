import React, { useEffect, useState, useRef } from "react";
import { Chart as ChartJS, RadialLinearScale, PointElement, LineElement, Filler, Tooltip, Legend } from 'chart.js';
import { Radar } from 'react-chartjs-2';

import * as tf from '@tensorflow/tfjs';
import * as faceDetection from '@tensorflow-models/face-detection';

ChartJS.register(
    RadialLinearScale,
    PointElement,
    LineElement,
    Filler,
    Tooltip,
    Legend
);

const Participant = ({ stream }) => {
    const videoRef = useRef();
    const canvasRef = useRef();

    const [detector, setDetector] = useState();
    const [model, setModel] = useState();

    const [modelsLoaded, setModelsLoaded] = useState(false);
    const [captureVideo, setCaptureVideo] = useState(false);

    // chart
    // const startTime = new Date().getTime();
    const [chartRenderKey, setChartRenderKey] = useState(0);
    const [radarData, setRadarData] = useState({
        labels: ['Surprised', 'Disgusted', 'Fearful', 'Sad', 'Angry', 'Happy', 'Neutral'],
        borderColor: 'rgba(255, 255, 255, 1)',
        redraw: true,
        datasets: [
            {
                label: '',
                data: [0, 0, 0, 0, 0, 0, 0],
                backgroundColor: 'rgba(255, 99, 132, 0.5)',
                borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1
            },
        ],
    });

    const [surprised, setSurprised] = useState(0);
    const [disgusted, setDisgusted] = useState(0);
    const [fearful, setFearful] = useState(0);
    const [sad, setSad] = useState(0);
    const [angry, setAngry] = useState(0);
    const [happy, setHappy] = useState(0);
    const [neutral, setNeutral] = useState(0);

    //== useEffect =============================================================
    useEffect(() => {
        window.currExpression = { start: null, end: null, value: 0, type: "" };
        loadModels();
    }, [])

    useEffect(() => {
        if (stream) {
            // video
            const videoTracks = stream.getVideoTracks();
            if (videoTracks.length > 0) {
                videoRef.current.srcObject = new MediaStream([videoTracks[0]]);
                videoRef.current.onloadedmetadata = () => { videoRef.current.play(); };
                setCaptureVideo(true);
            } else {
                if (videoRef?.current) videoRef.current.srcObject = null;
                setCaptureVideo(false);
            }
        } else {
            setCaptureVideo(false);
        }

        return () => {
            // eslint-disable-next-line
            if (videoRef?.current) videoRef.current.srcObject = null;
        }
        // eslint-disable-next-line
    }, [stream])

    useEffect(() => {
        if (modelsLoaded && captureVideo) {
            setTimeout(runFaceExpression);
        }
    }, [modelsLoaded, captureVideo])

    useEffect(() => {
        setRadarData({
            labels: ['Surprised', 'Disgusted', 'Fearful', 'Sad', 'Angry', 'Happy', 'Neutral'],
            borderColor: 'rgba(255, 255, 255, 1)',
            redraw: true,
            datasets: [
                {
                    label: '',
                    data: [surprised, disgusted, fearful, sad, angry, happy, neutral],
                    backgroundColor: 'rgba(255, 99, 132, 0.5)',
                    borderColor: 'rgba(255, 99, 132, 1)',
                    borderWidth: 1
                },
            ],
        });
    }, [surprised, disgusted, fearful, sad, angry, happy, neutral])

    useEffect(() => {
        setChartRenderKey(new Date().getTime());
    }, [radarData])

    const updateChart = (result) => {
        const now = new Date().getTime();
        if (result) {
            const expressions = result.expressions;
            let value = 0;
            let type = '';
            if (value < expressions.surprised) { type = "surprised"; value = expressions.surprised; }
            if (value < expressions.disgusted) { type = "disgusted"; value = expressions.disgusted; }
            if (value < expressions.fearful) { type = "fearful"; value = expressions.fearful; }
            if (value < expressions.sad) { type = "sad"; value = expressions.sad; }
            if (value < expressions.angry) { type = "angry"; value = expressions.angry; }
            if (value < expressions.happy) { type = "happy"; value = expressions.happy; }
            if (value < expressions.neutral) { type = "neutral"; value = expressions.neutral; }

            if (!window.currExpression.start) {
                // nếu là lần đầu tiên thì set mặc định
                window.currExpression = { start: now, end: now, value: 0, type: type }
            } else {
                if (window.currExpression.type === type) {
                    window.currExpression = { start: window.currExpression.start, end: now, value: 0, type: type }
                } else {
                    // nếu chuyển trạng thái
                    const delta = Math.round((now - window.currExpression.start) / 1000);
                    if (delta > 0) {
                        switch (window.currExpression.type) {
                            case "surprised": setSurprised(surprised + delta); break;
                            case "disgusted": setDisgusted(disgusted + delta); break;
                            case "fearful": setFearful(fearful + delta); break;
                            case "sad": setSad(sad + delta); break;
                            case "angry": setAngry(angry + delta); break;
                            case "happy": setHappy(happy + delta); break;
                            case "neutral": setNeutral(neutral + delta); break;
                        }
                    }

                    window.currExpression = { start: now, end: now, value: 0, type: type }
                }
            }
        } else if (window.currExpression.start) {
            const delta = Math.round((now - window.currExpression.start) / 1000);
            if (delta > 0) {
                switch (window.currExpression.type) {
                    case "surprised": setSurprised(surprised + delta); break;
                    case "disgusted": setDisgusted(disgusted + delta); break;
                    case "fearful": setFearful(fearful + delta); break;
                    case "sad": setSad(sad + delta); break;
                    case "angry": setAngry(angry + delta); break;
                    case "happy": setHappy(happy + delta); break;
                    case "neutral": setNeutral(neutral + delta); break;
                }
            }

            window.currExpression = { start: null, end: null, value: 0, type: "" }
        }
    }

    const runFaceExpression = async () => {
        if (canvasRef && canvasRef.current) {
            const canvasCtx = canvasRef.current.getContext('2d');
            canvasCtx.canvas.width  = videoRef.current.videoWidth;
            canvasCtx.canvas.height = videoRef.current.videoHeight;

            const estimationConfig = {flipHorizontal: false};

            const detection = await detector.estimateFaces(videoRef.current, estimationConfig);

            if (detection.length > 0) {
                const box = detection[0].box;

                box.yMin = Math.floor(box.yMin);
                box.xMin = Math.floor(box.xMin);

                box.yMax = Math.floor(box.yMax)+1;
                box.xMax = Math.floor(box.xMax)+1;
                
                box.yMin -= 20;

                box.xMin = Math.max(0, box.xMin);
                box.yMin = Math.max(0, box.yMin);

                const width = box.xMax - box.xMin;
                const height = box.yMax - box.yMin;

                const face_canvas = document.createElement('canvas');
                face_canvas.width = width;
                face_canvas.height = height;

                const face_canvas_ctx = face_canvas.getContext('2d');
                face_canvas_ctx.drawImage(videoRef.current, box.xMin, box.yMin, width, height, 0, 0, width, height);
                
                const tensor = tf.tidy(() => {
                    const img = tf.browser.fromPixels(face_canvas);
                
                    const normalizationOffset = tf.scalar(255/2);
                    const tensor = img
                            .resizeNearestNeighbor([112, 112])
                            .toFloat()
                            .sub(normalizationOffset)
                            .div(normalizationOffset)
                            .reverse(2)
                            .expandDims();
                    return tensor;
                });
                
                const predictions = await model.predict(tensor);

                const predict = predictions.dataSync();
                
                tensor.dispose();
                predictions.dispose();

                const expressions = {
                    angry: predict[0],
                    disgusted: predict[1],
                    fearful: predict[2],
                    happy: predict[3],
                    neutral: predict[4],
                    sad: predict[5],
                    surprised: predict[6]
                }
                
                const result = {expressions: expressions}

                updateChart(result);

                const label = ['angry', 'disgusted', 'fearful', 'happy', 'neutral', 'sad', 'surprised']
                let max = 0, expression;
                for (let i = 0;i < 7;i++){
                    if (predict[i] > max){
                        max = predict[i];
                        expression = label[i];
                    }
                }

                canvasCtx.strokeStyle = 'blue';
                canvasCtx.lineWidth = 2;
                canvasCtx.strokeRect(box.xMin, box.yMin, width, height);
                
                if (max >= 0.01){
                    canvasCtx.font = 'bold 24px verdana, sans-serif';
                    canvasCtx.fillStyle = '#228B22';
                    canvasCtx.fillText(expression, box.xMin, box.yMin - 5);
                }

                face_canvas.remove();

            } else {
            
            }

            setTimeout(runFaceExpression);
        }
    }

    const loadModels = async () => {
        const face_detect = faceDetection.SupportedModels.MediaPipeFaceDetector;
        const detectorConfig = {
            runtime: 'mediapipe',
            solutionPath: 'https://cdn.jsdelivr.net/npm/@mediapipe/face_detection'
        };
        
        const detector = await faceDetection.createDetector(face_detect, detectorConfig);

        const model = await tf.loadLayersModel('/models/model.json');
        // model.summary();

        setDetector(detector);
        setModel(model);

        setModelsLoaded(true);
    }

    //==================== RETURN ===========================================
    return (
        <div className="ani" style={{ display: "", background: "#000", height: "auto", width: "100%", position: "relative", textAlign: "center" }}>

            {/* Participant video */}
            <video
                ref={videoRef}
                autoPlay={true}
                muted={true}
                className="remote-video-par"
                style={{
                    width: "100%",
                    height: "auto",
                    maxHeight: "100vh"
                }}
            />

            <canvas ref={canvasRef}
                className="remote-video-par"
                style={{
                    width: "100%",
                    height: "auto",
                    maxHeight: "100vh"
                }}
            />

            <div style={{
                position: "fixed",
                bottom: "0px",
                right: "0px",
                width: "256px",
                height: "256px",
                background: "rgba(255,255,255, 1)"
            }}>
                <Radar key={chartRenderKey} data={radarData} />
            </div>
        </div>
    );
};

export default Participant;
