import React, { useState, useEffect } from "react";
import Participant from "../components/Participant";

function Calling({ ...props }) {
  const [localStream, setLocalStream] = useState(null);

  const startCameraStream = async () => {
    let streamError = 1;
    let streamOption = {
      video: {
        facingMode: "environment"
      }
    };

    let stream;
    while (streamError !== 0) {
      try {
        stream = await navigator.mediaDevices.getUserMedia(streamOption);
        window.localStream = stream;
        setLocalStream(stream);
        streamError = 0;
        console.log(`[Calling] startCameraStream - streamOption:`, streamOption);
      } catch (ex) {
        console.error(`[Calling] ex:`, ex);
        if (ex.message === "Permission denied") streamError++;
        if (streamError === 2) delete streamOption.video;
      }
    }
  }

  //======================================================================
  useEffect(() => {
    window.addEventListener('popstate', () => {
      if (localStream) {
        let tracks = localStream.getTracks();
        for (const track of tracks) {
          track.stop();
        }
      }
    });

    startCameraStream();

    return (() => {
    });
    // eslint-disable-next-line
  }, []);

  //======================================================================
  return (
    <div className="room" style={{ background: "#000", width: "100%", height: "100vh", overflow: "hidden" }}>
      <div style={{ display: "flex", width: "100%", height: "100vh", alignItems: "center" }}>
        {localStream && <Participant
          key={`local_${localStream.id}`}
          stream={localStream}
          isLocal={true}>
        </Participant>}
      </div>
    </div>
  );
}

export default Calling;
