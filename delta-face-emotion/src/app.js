import React, { useState, useEffect } from 'react';

const appRootData = {
    call_action: ""
}

export const appContext = React.createContext({
    call_action: ""
});


export default (props) => {
    const [appData, setAppData] = useState(appRootData);

    useEffect(() => {
    }, [appData])

    return (<appContext.Provider
        value={{
            appData: appData,
            setAppData: setAppData
        }}>
        {props.children}
    </appContext.Provider>
    );
}
