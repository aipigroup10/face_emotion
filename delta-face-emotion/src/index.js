import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";

// styles
import "assets/css/video-react.css";
import "assets/css/rts.css";
import "assets/css/custom.new.css";

// pages
import App from "./app";
import Calling from "views/Calling";

ReactDOM.render(
  <App>
    <BrowserRouter basename="/">
      <Switch>
        <Route exact path="/" render={props => <Calling {...props} />} />
      </Switch>
    </BrowserRouter>
  </App>,
  document.getElementById("root")
);