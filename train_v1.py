
"""
nohup python train_v1.py > train_v1.log 2>&1 &
"""

import os
import tensorflow
from tensorflow.keras.applications.mobilenet import  MobileNet
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2
from tensorflow.keras.layers import GlobalAveragePooling2D, Dense, Dropout
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from keras.callbacks import  EarlyStopping

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow import keras
from tensorflow.keras.callbacks import ModelCheckpoint
import tensorflowjs as tfjs

ROOT_DIR = "/home/ubuntu/HTV_DEV"

#ROOT_DIR = "/home/htv-namtn/HTV_DEV"

# config number class
MODEL_NAME = "face_emotion"
# 3. Make data
data_folder = f"{ROOT_DIR}/data"
tfjs_target_dir = f"{ROOT_DIR}/face_emotion_MobileNetV2"
n_class = 7


# {'angry': 0, 'disgusted': 1, 'fearful': 2, 'happy': 3, 'neutral': 4, 'sad': 5, 'surprised': 6}
# 2. Build model
def get_model():
    base_model = MobileNetV2(include_top=False, weights="imagenet", input_shape=(224,224,3))
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(1024, activation='relu')(x)
    #x = Dropout(0.25)(x)
    x = Dense(1024, activation='relu')(x)
    # x = Dropout(0.25)(x)
    x = Dense(512, activation='relu')(x)
    outs = Dense(n_class, activation='softmax')(x)

    # Đóng băng các layer của base_model
    # for layer in base_model.layers:
    #     layer.trainable = False
    print("Number layer:", len(base_model.layers))
    for layer in base_model.layers[:25]:
        layer.trainable=False
    for layer in base_model.layers[25:]:
        layer.trainable=True

    model = Model(inputs=base_model.inputs, outputs= outs)
    return model

model = get_model()
model.summary()



train_datagen = ImageDataGenerator(preprocessing_function= keras.applications.mobilenet.preprocess_input,rotation_range=0.2,
                                   width_shift_range=0.2,   height_shift_range=0.2,shear_range=0.3,zoom_range=0.5,
                                   horizontal_flip=True, vertical_flip=True,
                                   validation_split=0.2)

train_generator = train_datagen.flow_from_directory(data_folder,
                                                    target_size=(224, 224),
                                                    batch_size=64,
                                                    class_mode='categorical',
                                                    subset='training')

validation_generator = train_datagen.flow_from_directory(
    data_folder,  # same directory as training data
    target_size=(224, 224),
    batch_size=64,
    class_mode='categorical',
    subset='validation')  # set as validation data

classes = train_generator.class_indices
print(classes)
classes = list(classes.keys())

# 4. Train model
n_epochs = 50
number_k = 5
batch_size = 64
Adam=keras.optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=1e-5, amsgrad=False)
es = EarlyStopping(patience=50)
model.compile(optimizer=Adam, loss='categorical_crossentropy', metrics=['accuracy'])
os.makedirs(f"{ROOT_DIR}/models/{MODEL_NAME}", exist_ok=True)
checkpoint = ModelCheckpoint(f'{ROOT_DIR}/models/{MODEL_NAME}/model_best.hdf5', monitor='val_loss', save_best_only = True, mode='auto')
callback_list = [checkpoint]

step_train = train_generator.n//batch_size
step_val = validation_generator.n//batch_size


for i in range(number_k):
    model.fit_generator(generator=train_generator, steps_per_epoch=step_train,
                        validation_data=validation_generator,
                        validation_steps=step_val,
                        callbacks=callback_list,
                        epochs=n_epochs)


    print("Start save model")
    tfjs_target_dir_saved = tfjs_target_dir + f"_epoch{i*n_epochs}"
    tfjs.converters.save_keras_model(model, tfjs_target_dir_saved)
    print("Saved model: ", tfjs_target_dir_saved)
