from flask import Flask, render_template
from flask import Flask
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


@app.route("/", methods=['GET'])
def home_page():
    return render_template('index.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False)