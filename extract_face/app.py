from flask import Flask, jsonify, request, send_from_directory
import os
import cv2

app = Flask(__name__)

html = open('index.html').read()
@app.route('/')
def home():
    return html

js = open('script.js').read()
@app.route('/script.js')
def script():
    return js

@app.route('/images/<path:path>')
def send_files(path):
    abs_path = os.path.join('images', path)

    if os.path.isfile(abs_path):
        return send_from_directory('images', path)

    files = os.listdir(abs_path)
    return jsonify(files)

@app.route("/extract_face", methods=["POST"])
def extract_face():
    image = request.form['image']
    image = image[22:]

    expression = request.form['expression']

    xMin = request.form['xMin']
    xMin = int(float(xMin))

    yMin = request.form['yMin']
    yMin = int(float(yMin))

    xMax = request.form['xMax']
    xMax = int(float(xMax))+1

    yMax = request.form['yMax']
    yMax = int(float(yMax))

    save_name = request.form['save_name']
    #print(image, expression, xMin, yMin, xMax, yMax, save_name)

    try:
        img = cv2.imread(image)

        yMin = max(yMin, 0)
        xMin = max(xMin, 0)
        
        if (yMin >= 20):
            crop_img = img[(yMin-20):yMax, xMin:xMax]
        else:
            crop_img = img[yMin:yMax, xMin:xMax]
        
        cv2.imwrite('extracted/' + expression + '/' + save_name, crop_img)
    except:
        print("error", image, xMin, yMin, xMax, yMax)

    return "success", 200

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5500, debug=True)