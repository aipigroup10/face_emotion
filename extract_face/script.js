let face_detect;

async function loadModel(){
    const face_detection = faceDetection.SupportedModels.MediaPipeFaceDetector;
    const detectorConfig = {
    runtime: 'mediapipe',
        solutionPath: 'https://cdn.jsdelivr.net/npm/@mediapipe/face_detection',
    };
    face_detect = await faceDetection.createDetector(face_detection, detectorConfig);

    console.log('model loaded');
}

loadModel();

let cnt = 0;
let cantDetect;

function loadData(expressions){
    cnt = 0;
    cantDetect = 0;

    var dir = 'images/' + expressions + '/';
    
    $.get(dir, function(data){
        cnt = data.length;
        for (let i = 0;i < cnt;i++){
            let filename = data[i];
            $('#listImg').append('<img id="' + expressions + '_img_' + (i+1) + '" src="' + dir + filename + '"/>').hide();
        }

        console.log(expressions, cnt, 'images');
    });
}

async function detect(expression){
    console.log(new Date());

    for (let i = 0;i < cnt;i++){
        const image = document.getElementById(expression + '_img_' + (i+1));

        let img_name = image.src;
        img_name = img_name.substr(img_name.lastIndexOf('/') + 1);
        img_name = img_name.slice(0, img_name.lastIndexOf('.'));
        
        const predictions = await face_detect.estimateFaces(image, false);

        if (predictions.length > 1)
            console.log(predictions.length);
        
        try {
            let face;
            let max = 0, vt = -1;
            for (let j = 0;j < predictions.length;j++){
                face = predictions[j].box;
                let size = face.width * face.height;

                if ((face.width < 120) || (face.height < 120))
                    continue;

                if (size > max){
                    max = size;
                    vt = j;
                }
            }

            if (vt != -1){
                face = predictions[vt].box;
                $.post( "/extract_face", {
                    image: image.src,
                    expression: expression,
                    xMin: face.xMin,
                    yMin: face.yMin,
                    xMax: face.xMax,
                    yMax: face.yMax,
                    save_name: 'Dataset_' + img_name + '.jpg'
                });
            }
        }
        catch(err){
            cantDetect++;
            console.log('cantDetect', image.src);
        }
    }

    console.log(new Date());
}