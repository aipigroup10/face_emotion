import pandas as pd
import csv

from sklearn.metrics import confusion_matrix

dt = pd.read_csv("test.csv", delimiter=',')

face = dt.face

y_test = []
y_pred = []

for i in range(len(face)):
    if face[i] == 'OK':
        y_test.append(int(dt.label[i]))
        y_pred.append(int(dt.predict[i]))

f = open('output.csv', 'w', encoding='UTF8', newline='')
writer_csv = csv.writer(f)

c = confusion_matrix(y_test, y_pred, labels=[0, 1, 2, 3, 4, 5, 6])

writer_csv.writerows(c)

cnt = 0
for i in range(len(y_test)):
    if y_test[i] == y_pred[i]:
        cnt += 1

print(cnt, len(y_test), cnt/len(y_test))
print(c)

f.close()
