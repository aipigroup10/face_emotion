let face_detect;
let model;

async function loadModel(){
    const detectorConfig = {
        runtime: 'tfjs'
    };
    face_detect = await faceDetection.createDetector('MediaPipeFaceDetector', detectorConfig);

    model = await tf.loadLayersModel('model/model.json');

    console.log('model loaded');
}

loadModel();

let cnt = 0;
let angry = 0, disgusted = 0, fearful = 0, happy = 0, neutral = 0, sad = 0, surprised = 0, cantDetect = 0;

function loadData(expressions){
    cnt = 0;
    cantDetect = 0;

    var dir = 'images/' + expressions + '/';
    
    $.get(dir, function(data){
        cnt = data.length;
        for (let i = 0;i < cnt;i++){
            let filename = data[i];
            $('#listImg').append('<img id="' + expressions + '_img_' + (i+1) + '" src="' + dir + filename + '"/>').hide();
        }

        console.log(expressions, cnt, 'images');

        switch (expressions) {
            case 'angry':
                angry = cnt;
                break;
            case 'disgusted':
                disgusted = cnt;
                break;
            case 'fearful':
                fearful = cnt;
                break;
            case 'happy':
                happy = cnt;
                break;
            case 'neutral':
                neutral = cnt;
                break;
            case 'sad':
                sad = cnt;
                break;
            case 'surprised':
                surprised = cnt;
                break;

            default:
                break;
        }
    });
}

loadData('angry');
loadData('disgusted');
loadData('fearful');
loadData('happy');
loadData('neutral');
loadData('sad');
loadData('surprised');

let csvFileData = []

function download_csv_file() { 
    var csv = 'file_path,label,label_name,predict,predict_name,id,face\n';

    csvFileData.forEach(function (row) {
        csv += row.join(',');
        csv += "\n";
    });

    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';

    hiddenElement.download = 'test.csv';
    hiddenElement.click();
}

function label_to_code(label){
    switch (label){
        case 'angry':
            return 0;
        case 'disgusted':
            return 1;
        case 'fearful':
            return 2;
        case 'happy':
            return 3;
        case 'neutral':
            return 4;
        case 'sad':
            return 5;
        case 'surprised':
            return 6;
        case 'cant detect':
            return -1;
    }
}

let d = 0;

async function evaluate(expression){
    let n = 0;
    switch (expression) {
        case 'angry':
            n = angry;
            break;
        case 'disgusted':
            n = disgusted;
            break;
        case 'fearful':
            n = fearful;
            break;
        case 'happy':
            n = happy;
            break;
        case 'neutral':
            n = neutral;
            break;
        case 'sad':
            n = sad;
            break;
        case 'surprised':
            n = surprised;
            break;

        default:
            break;
    }

    for (let i = 0;i < n;i++){
        const image = document.getElementById(expression + '_img_' + (i+1));

        let img_name = image.src;
        img_name = img_name.substr(img_name.lastIndexOf('/') + 1);

        let id = img_name.substr(img_name.lastIndexOf('_') + 1);
        id = id.slice(0, id.indexOf('.'));
        
        const detection = await face_detect.estimateFaces(image, false);
        
        if (detection.length > 0){
            const box = detection[0].box;

            box.yMin = Math.floor(box.yMin);
            box.xMin = Math.floor(box.xMin);

            box.yMax = Math.floor(box.yMax)+1;
            box.xMax = Math.floor(box.xMax)+1;
            
            box.yMin -= 20;

            box.xMin = Math.max(0, box.xMin);
            box.yMin = Math.max(0, box.yMin);

            const width = box.xMax - box.xMin;
            const height = box.yMax - box.yMin;

            const face_canvas = document.createElement('canvas');
            face_canvas.width = width;
            face_canvas.height = height;

            const face_canvas_ctx = face_canvas.getContext('2d');
            face_canvas_ctx.drawImage(image, box.xMin, box.yMin, width, height, 0, 0, width, height);
            
            const tensor = tf.tidy(() => {
                const img = tf.browser.fromPixels(face_canvas);
            
                const normalizationOffset = tf.scalar(255/2);
                const tensor = img
                        .resizeNearestNeighbor([112, 112])
                        .toFloat()
                        .sub(normalizationOffset)
                        .div(normalizationOffset)
                        .reverse(2)
                        .expandDims();
                return tensor;
            });
            
            const predictions = await model.predict(tensor);

            const e = dominantExpression(predictions.dataSync());

            let row = [image.src.slice(29), label_to_code(expression), expression, label_to_code(e), e, id, 'OK'];
            csvFileData.push(row);

            //$('body').append(face_canvas);
            face_canvas.remove();

            tensor.dispose();
            predictions.dispose();
        }
        else {
            let row = [image.src.slice(29), label_to_code(expression), expression, '', '', id, 'NO_FACE'];
            csvFileData.push(row);
        }
        console.log(expression, i);
    }

    $('#res').append('<div>'+ expression + ': ' + n + ' images' + '</div>');
    d++;
    if (d == 7){
        d = 0;
        $('#wait').remove();
        $('#res').append('<button id="dl_csv" onclick="download_csv_file()">Download csv</button>');
        $('#dl_csv').click();
    }
}

function dominantExpression(detections){
    if (detections !== undefined){
        const expressions = detections;
        let arr = ['angry', 'disgusted', 'fearful', 'happy', 'neutral', 'sad', 'surprised']
        let max = 0, res;
        for (let i = 0;i < 7;i++){
            if (expressions[i] > max){
                max = expressions[i];
                res = arr[i];
            }
        }

        return res;
    }

    return 'cant detect';
}

window.onload = () => {
    $('#load').text('Loaded');

    $('#res').append('<div id="wait">Processing...please wait...</div>');
    setTimeout(() => {
        evaluate('angry');
        evaluate('disgusted');
        evaluate('fearful');
        evaluate('happy');
        evaluate('neutral');
        evaluate('sad');
        evaluate('surprised');}, 1500)
};
