from flask import Flask, jsonify, request, send_from_directory
import os

app = Flask(__name__)

html = open('index.html').read()
@app.route('/')
def home():
    return html

@app.route('/<path:path>')
def send_files(path):
    if os.path.isfile(path):
        return send_from_directory(os.path.dirname(path), os.path.basename(path))
    if path == 'favicon.ico':
        return {'error': 404}
    files = os.listdir(path)
    return jsonify(files)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5500, debug=True)