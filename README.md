# Face Emotion Trainer
Face Emotion Training and DEMO and Evaluation 

## 1. Training model
1. Prepare data in /home/ubuntu/HTV_DEV/data 
```
ubuntu@ip-10-0-1-228:~/HTV_DEV/face_emotion_trainer$ tree  /home/ubuntu/HTV_DEV/data/ -L 1
/home/ubuntu/HTV_DEV/data/
├── angry
├── disgusted
├── fearful
├── happy
├── neutral
├── sad
└── surprised

7 directories, 0 files
```
2. Run Training 
```
$ cd /home/ubuntu/HTV_DEV/face_emotion_trainer
$ nohup python train_v6.py > train_v8.log 2>&1 &

```
## 2. Evaluation model 
- Copy evaluation data to "Evaluation/images"
```
$ cd Evaluation
$ cp -r /home/ubuntu/HTV_DEV/data_test/CustomerDataLabel_20220915 images

OUTPUT: ~/HTV_DEV/face_emotion_trainer/Evaluation$ tree images/ -L 1
images/
├── anger
├── disgust 
├── fear
├── happy
├── neutral
├── sad
└── surprise

```
- Copy model to "Evaluation/model"
```
$ rm -rf model
$ cp -r /home/ubuntu/HTV_DEV/fe_MobileNet_exp07_epoch50 model


OUTPUT: tree . -L 1
.
├── app.py
├── confusion_matrix.py
├── images
├── index.html
├── model
└── script.js

2 directories, 4 files


```
- Run Serviecs Test


```
$ python3 app.py

```


## 3. Run Face Emotion Demo
- Setup npm and node tested on npm@8.15.0, node: v16.17.0

```
$ sudo apt-get remove nodejs
$ sudo apt-get remove npm
$ sudo apt-get install curl
$ curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash -

$ sudo apt-get install nodejs
$ node -v 
$ sudo npm install -g n

```

- Copy model in 'delta-face-api_test/public/models'

```
$ cp model_dir/* delta-face-api_test/public/models/
$ npm install
$ npm start
```


